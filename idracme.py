#!/usr/bin/env python3

import os
import platform
import argparse
import getpass
import requests
import xmltodict
import json
import subprocess

from shutil import copyfile
from pprint import pprint
from datetime import datetime
from urllib3.exceptions import InsecureRequestWarning

# ./jre/bin/java -cp avctKVM.jar -Djava.library.path=./lib com.avocent.idrac.kvm.Main ip=$drachost kmport=5900 vport=5900 user=$dracuser passwd=$dracpwd apcp=1 version=2 vmprivilege=true "helpurl=https://$drachost:443/help/contents.html"

cur_os = platform.system()
cur_arch = platform.machine()
__location__ = os.path.abspath(os.path.dirname(__file__))
 
def connect(host, username, password, host_directory):
    cmd = ['exec %s/jre/bin/java' % (host_directory),
                '-cp',
                '%s/avctKVM.jar' % (host_directory),
                '-Djava.library.path=./lib',
                'com.avocent.idrac.kvm.Main',
                'ip=%s' % (host),
                'kmport=5900',
                'vport=5900',
                'user=%s' % (username),
                'passwd=%s' % (password),
                'apcp=1',
                'version=2',
                'vmprivilege=true',
                'helpurl=https://%s:443/help/contents.html' % (host)
    ]
    subprocess.call(' '.join(cmd), shell=True)

def gather_files(urls, hostname, dest):
    requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

    for url in urls:
        vprint(url)
        filename = url.split('/')[-1]
        response = requests.get(url, allow_redirects=True, verify=False, auth=(username, password))
        open('%s/%s' % (dest, filename), 'wb').write(response.content)

def get_urls(viewer_file):
    urls = list()
    with open(viewer_file, 'r') as fh:
        o = xmltodict.parse(fh.read())
        for i in o['jnlp']['resources']:
            try:
                urls.append(i['jar']['@href'])
            except KeyError:
                pass

            try:
                if i['@os'] == cur_os and i['@arch'] == cur_arch:
                    for j in i['nativelib']:
                        urls.append(j['@href'])
            except KeyError:
                pass
    return urls

parser = argparse.ArgumentParser(description='Connect to idrac6')
parser.add_argument('host', type=str, help='Remote iDrac6 endpoint')
parser.add_argument('username', type=str, help="Username for remote iDrac6 endpoint")
parser.add_argument('filepath', type=str, help="Path to viewer.jlnp file")
parser.add_argument('-v', '--verbose', help='Increse verbosity of output', action='store_true')

args = parser.parse_args()

if args.verbose:  # if the debug flag is set then create a verbosity function
    def vprint(*args): # vprint a.k.a verbose print
        for arg in args:
            now = datetime.now()
            print(str(now).ljust(25, ' ') + ': ' + arg)
else:
    vprint = lambda *a: None #  or else create a nothing function

host = args.host
username = args.username
password = getpass.getpass()

host_directory = os.path.join(__location__, host)
viewer_file = os.path.abspath(args.filepath)
print(viewer_file)

try:
    vprint('Creating new dictory for jre files')
    os.mkdir(host_directory)
except FileExistsError as err:
    vprint('Skipping creation of directory %s. It ready exists...' % (host_directory))

try:
    vprint('Create a symlink: %s -> %s/jre' % (__location__, host_directory))
    os.symlink('%s/jre' % (__location__), '%s/jre' % (host_directory))
except FileExistsError:
    vprint('Skipping creation of symlink %s -> %s/jre. It already exists...' % (__location__, host_directory))

vprint('Copying file %s to %s' % (viewer_file, host_directory))
copyfile(viewer_file, os.path.join(host_directory, os.path.basename(viewer_file)))
new_viewer_file = os.path.join(host_directory, os.path.basename(viewer_file))

vprint('Gathering library files and placing them in %s' % (host_directory))
urls = get_urls(new_viewer_file)

gather_files(urls, host, host_directory)
connect(host, username, password, host_directory)

