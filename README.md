# Portable iDrac console launcher

This is a wrapper script I wrote for launching Java based iDrac consoles. I took inspiration from https://gist.github.com/xbb/4fd651c2493ad9284dbcb827dc8886d6.


# Installation

No installation is required, just clone this repo and use the `idracme.py` script.

# Usage


Download the viewer.jnlp file from a iDrac endpoint and use this script to launch a console with it. This file is downloaded when you try to launch a console. The script take in three parameters; hostname, username and filepath.
The filepath should the an absolute path to the `viewer.jnlp` file. (Sometimes these filenames have alot of meta information appended to them resulting in long names).

For help use:
`./idracme.py -h`

## Working idrac versions
This script has successfully been tested on idrac6 and idrac7 endpoints.

# Licence
```

idrac-jre-wrapper - Portable JRE connector for idrac6            
Copyright (c) 2019 Ben Kooijman <ben@pin.se>                    
                                                                         
This program is free software: you can redistribute it and/or modify     
it under the terms of the GNU General Public License as published by     
the Free Software Foundation, either version 3 of the License, or        
(at your option) any later version.                                      
                                                                         
This program is distributed in the hope that it will be useful,          
but WITHOUT ANY WARRANTY; without even the implied warranty of           
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            
GNU General Public License for more details.                             
                                                                         
You should have received a copy of the GNU General Public License        
along with this program.  If not, see <https://www.gnu.org/licenses/>.  
```